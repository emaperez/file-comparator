# frozen_string_literal: true

class CreateComparators < ActiveRecord::Migration[6.0]
  def change
    create_table :comparators do |t|
      t.string :jsonfile1
      t.string :jsonfile2
      t.boolean :equal
      t.string :diff

      t.timestamps
    end
  end
end
