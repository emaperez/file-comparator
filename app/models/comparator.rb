# frozen_string_literal: true

require 'json-diff'
require 'json'

class Comparator < ApplicationRecord
  attr_accessor :fileContent1
  attr_accessor :fileContent2
  validates :jsonfile1, :jsonfile2, presence: true
  before_save :compute_values

  private
    def compute_values
      firstJson = JSON.parse(fileContent1.read)
      secondJson = JSON.parse(fileContent2.read)
      computedJsonDiff = JsonDiff.diff(firstJson, secondJson)
      self.diff = !computedJsonDiff.length.zero? ? computedJsonDiff : nil
      self.equal = diff.nil?
    end
end
