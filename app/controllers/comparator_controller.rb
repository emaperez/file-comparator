# frozen_string_literal: true

class ComparatorController < ApplicationController
  def index
    @comparator = Comparator.new
  end

  def create
    @comparator = Comparator.new
    @comparator.jsonfile1 = !params[:jsonfile1].nil? ? params[:jsonfile1].original_filename : nil
    @comparator.jsonfile2 = !params[:jsonfile2].nil? ? params[:jsonfile2].original_filename : nil
    @comparator.fileContent1 = params[:jsonfile1]
    @comparator.fileContent2 = params[:jsonfile2]

    if @comparator.save
      logger.debug "The files are equal: #{@comparator.equal}, file 1:#{@comparator.jsonfile1}, file 2:#{@comparator.jsonfile2}"
      @equal = @comparator.equal
      @diff = @comparator.diff
      render 'result'
    else
      render 'index'
    end
  end
end
