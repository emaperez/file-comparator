# README

This is a quite simple web application that allows the user to upload 2 json files and compare them, returning
whether they are equal or not and their differences.
It is my very first encounter with Ruby and Rails which means that there are are probably a ton of things to improve
and do better.
The JSON comparison was done using 'json-diff'. Instead of reinventing the wheel and coding a new algorithm to do it,
I thought it was better to reuse existing libraries/gems.
Lint issues were fixed automatically running Rubocop https://rubocop.org/

The following enhancements can be made if more time is spent on the project (I devoted just a couple of hours as suggested):
  1. Using partials and AJAX to show the comparison result without redirecting the user to a different page.
  2. Better UI and cleaner CSS/scss
  3. Unit tests
  4. Etc


* Ruby version

  Using Ruby '2.7.2'

* System dependencies

   - Execute '_bundle install_' to install all gems
   - Run '_yarn install_'

* Application execution

Run '_rails server_' from the command line and Puma web server will launch the application
Then open your browser and type 'http://localhost:3000/'